const Api = () => {
    return fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=coffee')
        .then(jsonData => jsonData?.json())
        .then(data => data?.drinks)
}

export default Api;
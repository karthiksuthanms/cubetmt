import {NativeStackScreenProps} from '@react-navigation/native-stack';
import { TextStyle, ViewStyle } from 'react-native';

export type stackNavParamList = {
  Login: undefined;
  Listing: undefined;
  Details: undefined | {item: any};
};

export type headerProps = {
  onBackPress?: () => void;
  title: string;
};

export type listTileProp = {
  item: any;
  index: number;
  callback: () => void;
};

export type buttonProps = {
  title: string;
  disabled: boolean;
  onPress: () => void;
};

export type textBoxProps = {
  title?: string;
  secureTextEntry?: boolean;
  value: string;
  onChangeText: (e:string) => void;
  onFocus: () => void;
  onBlur: () => void;
  placeholder: string;
  editable: boolean;
  style : any;
};

export type loginProps = NativeStackScreenProps<stackNavParamList, 'Login'>;
export type navProps = NativeStackScreenProps<stackNavParamList, 'Details'>;
export type listingProps = NativeStackScreenProps<stackNavParamList, 'Listing'>;

export const CustomColors = {
    PrimaryTextColor:'#8C746A',
    TextLight:'#B98068',
    TextExtraLight:'rgba(62, 74, 89, 0.42)',
    SecondaryTextColor:'#3E4A59',
    borderColor:'#D3DFEF',
    borderFocus:'#C28E79',
    White:'#FFFFFF',
    tileTitle:'#2D140D',
}
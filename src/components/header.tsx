import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { CustomColors } from '../assets/CustomColors';
import { CustomFont } from '../assets/Fonts';
import { Images } from '../assets/Images';
import { headerProps } from '../common/typeDefs';
import { logoutSuccess } from '../store/actions/homeActions';

const Header =(props:headerProps) =>{
    return (
        <View style={styles.headerWrap}>
            <TouchableOpacity style={styles.buttonWrap} onPress={props.onBackPress} disabled={!props.onBackPress}>
                <Image source ={Images.back} style={styles.back}/>
            </TouchableOpacity>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.logout} onPress={logoutSuccess}>Logout</Text>
        </View>
    )
}

export default Header;

const styles = StyleSheet.create({
    headerWrap:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:30,
        paddingVertical:20,
        borderBottomWidth:1,
        borderColor:CustomColors.borderColor,
    },
    buttonWrap:{

    },
    title:{
        fontFamily:CustomFont,
        fontSize:18,
        lineHeight:26,
        color:CustomColors.PrimaryTextColor,
        fontWeight:'600',
        marginLeft:25,
    },
    logout:{
        fontFamily:CustomFont,
        fontSize:18,
        lineHeight:26,
        color:CustomColors.PrimaryTextColor,
        fontWeight:'600',
    },
    back:{
        height:15,
        width:15,
        resizeMode:'contain',
    }
})
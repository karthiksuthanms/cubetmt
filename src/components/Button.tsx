import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { CustomColors } from '../assets/CustomColors';
import { CustomFont } from '../assets/Fonts';
import { buttonProps } from '../common/typeDefs';

const Button =(props:buttonProps) =>{
    return (
        <TouchableOpacity style={[styles.button,props.disabled && styles.disabled]} onPress={props.onPress} disabled={props.disabled}>
            <Text style={styles.title}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default Button;

const styles = StyleSheet.create({
    button:{
        backgroundColor:CustomColors.TextLight,
        borderRadius:25,
        height:50,
        alignItems:'center',
        justifyContent:'center',
        marginTop:40,
        marginBottom:15,
},
    title:{
        fontFamily:CustomFont,
        fontSize:14,
        lineHeight:16,
        color:CustomColors.White,
        fontWeight:'400',
    },
    disabled:{
        opacity:0.5,
    }
})
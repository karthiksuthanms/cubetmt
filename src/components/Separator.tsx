import React, { memo } from 'react'
import { View } from 'react-native'

const Separator =memo(() =>{
    return (
        <View style={{height:1,width:'100%',backgroundColor:'#E6E1DE'}}/>
    )
})

export default Separator;
import React, { memo } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { CustomColors } from '../assets/CustomColors';
import { CustomFont } from '../assets/Fonts';
import { Images } from '../assets/Images';
import { listTileProp } from '../common/typeDefs';

const ListElement =memo((props:listTileProp) =>{
    const {item,index,callback} = props;
    return (
        <TouchableOpacity style={styles.tileWrap} onPress={callback}>
            <View style={styles.wrap1}>
                <Image source={{uri:item?.strDrinkThumb}} style={styles.tileImage}/>
                <Text style={styles.title}>{item?.strDrink}</Text>
            </View>
            <Image source={Images.back} style={styles.rightArrow}/>
        </TouchableOpacity>
    )
})

export default ListElement;

const styles = StyleSheet.create({
    tileWrap:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:20,
        paddingVertical:20,
    },
    wrap1:{
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:15,
    },
    title:{
        fontFamily:CustomFont,
        fontSize:20,
        lineHeight:24,
        color:CustomColors.tileTitle,
        fontWeight:'600',
        marginLeft:30,
    },
    tileImage:{
        height:50,
        width:50,
        resizeMode:'contain',
        borderRadius:25,
    },
    rightArrow:{
        height:20,
        width:20,
        resizeMode:'contain',
        transform:[{rotate:'180deg'}]
    },
})
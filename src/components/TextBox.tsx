import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { CustomColors } from '../assets/CustomColors';
import { CustomFont } from '../assets/Fonts';
import { textBoxProps } from '../common/typeDefs';

const TextBox = (props:textBoxProps) => {
    return (
        <View style={ styles.wrap }>
            {props.title && <Text style={styles.inputHeading}>{props.title}</Text>}
            <TextInput
                style={[!props.secureTextEntry ?styles.input : styles.password, props.style]}
                value={props.value}
                onChangeText={props.onChangeText}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                placeholder={props.placeholder}
                secureTextEntry={props.secureTextEntry}
                editable={props.editable}
            />
        </View>
    )
}

export default TextBox;

const styles = StyleSheet.create({
    wrap: {
        borderBottomWidth: 1,
        borderBottomColor: CustomColors.borderColor,
    },
    inputHeading: {
        fontFamily: CustomFont,
        opacity: 0.7,
        fontSize: 14,
        lineHeight: 16,
        color: CustomColors.SecondaryTextColor,
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: CustomColors.White,
        fontFamily: CustomFont,
        fontSize: 18,
        lineHeight: 24,
        fontWeight: '500',
        // backgroundColor:'pink',
        paddingVertical:10,
        paddingHorizontal:0,
    },
    password: {
        borderBottomWidth: 1,
        borderBottomColor: CustomColors.White,
        fontFamily: CustomFont,
        fontSize: 14,
        lineHeight: 16,
        fontWeight: '400',
        // backgroundColor:'pink',
        paddingVertical:10,
        paddingHorizontal:0,
    },
})
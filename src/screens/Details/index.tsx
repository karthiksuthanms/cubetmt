import {NativeStackScreenProps} from '@react-navigation/native-stack/lib/typescript/src/types';
import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {CustomColors} from '../../assets/CustomColors';
import {CustomFont} from '../../assets/Fonts';
import {navProps, stackNavParamList} from '../../common/typeDefs';
import Header from '../../components/header';

const App = ({navigation, route}: navProps) => {
  const item = route?.params?.item;
  console.log(item);
  return (
    <SafeAreaView style={styles.safeArea}>
      <Header title="Detail" onBackPress={() => navigation.goBack()} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <View>
          <Text style={styles.heading}>{item?.strDrink}</Text>
          <Image source={{uri: item?.strDrinkThumb}} style={styles.Image} />
          <Text
            style={
              styles.subHeading
            }>{`${item?.strAlcoholic} - ${item?.strCategory} `}</Text>
          <View style={styles.row}>
            <Text style={styles.heading2}>Instructions</Text>
            <Text style={styles.body}>{item?.strInstructions}</Text>
            <Text style={styles.body}>{item?.strInstructionsDE}</Text>
            <Text style={styles.body}>{item?.strInstructionsIT}</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: CustomColors?.White,
    justifyContent: 'space-between',
  },
  Image: {
    height: 250,
    width: 250,
    resizeMode: 'contain',
    borderRadius: 25,
    marginTop: 30,
    alignSelf: 'center',
  },
  heading: {
    fontFamily: CustomFont,
    fontSize: 24,
    lineHeight: 28,
    color: CustomColors.TextLight,
    fontWeight: '800',
    textAlign: 'center',
    marginTop: 15,
  },
  subHeading: {
    fontFamily: CustomFont,
    fontSize: 16,
    lineHeight: 18,
    color: CustomColors.TextLight,
    fontWeight: '500',
    textAlign: 'center',
    marginTop: 15,
  },
  heading2: {
    fontFamily: CustomFont,
    fontSize: 14,
    lineHeight: 16,
    color: CustomColors.SecondaryTextColor,
    fontWeight: '600',
    textAlign: 'center',
    marginTop: 15,
    textDecorationLine: 'underline',
  },
  body: {
    fontFamily: CustomFont,
    fontSize: 16,
    lineHeight: 20,
    color: CustomColors.SecondaryTextColor,
    fontWeight: '400',
    textAlign: 'center',
    marginTop: 15,
    marginHorizontal: 25,
  },
  row: {
    marginBottom: 25,
  },
});

export default App;

import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {loginSuccess} from '../../store/actions/homeActions';
import {CustomColors} from '../../assets/CustomColors';
import {CustomFont} from '../../assets/Fonts';
import Button from '../../components/Button';
import TextBox from '../../components/TextBox';
import {credentials} from '../../common/constants';
import {loginProps} from '../../common/typeDefs';

const App = ({navigation, route}: loginProps) => {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [focus, setFocus] = useState(0);
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const handleLogin = () => {
    setDisabled(true);
    if (
      userName === credentials.userName &&
      password === credentials.password
    ) {
      loginSuccess();
    } else {
      setDisabled(false);
    }
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Text style={styles.login}>Login</Text>
      <Text style={styles.heading}>Welcome back!</Text>
      <TextBox //custom textBox defined at ../../components/TextBox
        title="Email"
        value={userName}
        style={focus === 1 && styles.borderHighlight}
        onChangeText={(e: string) => setUserName(e)}
        onFocus={() => setFocus(1)}
        onBlur={() => setFocus(0)}
        editable={!disabled}
        placeholder="abcd@example.com"
      />
      <TextBox
        value={password}
        style={focus === 2 && styles.borderHighlight}
        onChangeText={(e: string) => setPassword(e)}
        onFocus={() => setFocus(2)}
        onBlur={() => setFocus(0)}
        placeholder="Password"
        secureTextEntry
        editable={!disabled}
      />
      <Text style={styles.forgot}>Forgot password?</Text>
      <Button title="Log In" onPress={handleLogin} disabled={disabled} />
      {/*custom Button defined at ../../components/Button*/}
      <Text style={styles.footerText}>
        Don't have an account?
        <Text onPress={() => {}} style={styles.register}>
          Register
        </Text>
      </Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 30,
  },
  login: {
    fontFamily: CustomFont,
    fontSize: 22,
    lineHeight: 26,
    textAlign: 'center',
    color: CustomColors.PrimaryTextColor,
    fontWeight: '500',
    marginTop: 10,
  },
  heading: {
    fontFamily: CustomFont,
    fontSize: 32,
    lineHeight: 36,
    color: CustomColors.PrimaryTextColor,
    fontWeight: '500',
    marginVertical: 35,
  },
  // inputHeading:{
  //   fontFamily:CustomFont,
  //   opacity:0.7,
  //   fontSize:14,
  //   lineHeight:16,
  //   color:CustomColors.SecondaryTextColor,
  // },
  // input:{
  //   borderBottomWidth:1,
  //   borderBottomColor:CustomColors.borderColor,
  // },
  borderHighlight: {
    borderBottomWidth: 1,
    borderBottomColor: CustomColors.borderFocus,
  },
  forgot: {
    fontFamily: CustomFont,
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'right',
    color: CustomColors.TextLight,
    fontWeight: '500',
    marginTop: 20,
  },
  footerText: {
    fontFamily: CustomFont,
    fontSize: 14,
    lineHeight: 14,
    textAlign: 'center',
    color: CustomColors.TextExtraLight,
    fontWeight: '400',
  },
  register: {
    color: CustomColors.TextLight,
    fontWeight: '500',
  },
});

export default App;

import React, {useEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {CustomColors} from '../../assets/CustomColors';
import Api from '../../common/Api';
import {listingProps} from '../../common/typeDefs';
import Header from '../../components/header';
import ListElement from '../../components/ListElement';
import Separator from '../../components/Separator';

const App = ({navigation, route}: listingProps) => {
  const [data, setData] = useState<any[]>([]);
  const [error, setError] = useState('');

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    Api().then(result => {
      if (Array.isArray(result)) {
        setData(result);
      } else setError('Unable to Fetch , Something went wrong');
    });
  };

  const navigateToDetail = (item: any) => {
    navigation.navigate('Details', {item});
  };

  const renderItem = (prop: any) => {
    const {item, index} = prop;
    return (
      <ListElement
        item={item}
        index={index}
        callback={() => navigateToDetail(item)}
      />
    );
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header title="Menu" />
      <FlatList
        data={data}
        keyExtractor={(item, index) => item?.idDrink ?? index.toString()}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={Separator}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: CustomColors.White,
    justifyContent: 'space-between',
  },
});

export default App;

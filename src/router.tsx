import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './screens/login';
import Listing from './screens/Listing';
import Details from './screens/Details';
import { stackNavParamList } from './common/typeDefs';
import { StatusBar } from 'react-native';
import { useAppSelector } from './common/hooks';

const Stack = createNativeStackNavigator<stackNavParamList>();

function App(){
    const appReducer = useAppSelector(s=>s?.loggedIn)
    return(
        <NavigationContainer>
            <StatusBar barStyle='dark-content' backgroundColor='#FFFFFF'/>
            {appReducer ?
            <Stack.Navigator>
                <Stack.Screen name="Listing" component = {Listing} options={{headerShown:false}}/>   
                <Stack.Screen name="Details" component={Details} options={{ headerShown: false }} />   
            </Stack.Navigator> 
            :
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            </Stack.Navigator> 
            }
        </NavigationContainer>
    )
}

export default App;
import { createSlice } from "@reduxjs/toolkit";

export const AppSlice= createSlice({
    name:'home',
    initialState:{
        loggedIn:false,
    },
    reducers:{
        login:(state)=>{
            state.loggedIn=true;
        },
        logout: (state) => {
            state.loggedIn=false;
        },
    },
})

export const {login, logout} = AppSlice.actions;

export default AppSlice.reducer;
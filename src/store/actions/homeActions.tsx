import {login, logout } from '../reducerSlices/appSlice';
import { store } from "../store";


export const loginSuccess =()=>{
    store?.dispatch(login())
}

export const logoutSuccess = () => {
    store?.dispatch(logout())
}
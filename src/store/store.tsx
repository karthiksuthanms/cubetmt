import { combineReducers, configureStore } from '@reduxjs/toolkit'
import appSlice from './reducerSlices/appSlice'
import logger from 'redux-logger'
import AsyncStorage from '@react-native-async-storage/async-storage'
import hardSet from 'redux-persist/es/stateReconciler/hardSet'
import { FLUSH, PAUSE, PERSIST, persistReducer, persistStore, PURGE, REGISTER, REHYDRATE } from 'redux-persist'

const rootPersistConfig = {
    key:'authType',
    storage:AsyncStorage,
    stateReconciler:hardSet,
}

const persistedReducer = persistReducer<ReturnType<typeof appSlice>>(rootPersistConfig, appSlice);

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }).concat(logger)
})

const persistor = persistStore(store)

export {store,persistor}

export type RootStateType = ReturnType<typeof store.getState>;
export type AppDispatchType = typeof store.dispatch;